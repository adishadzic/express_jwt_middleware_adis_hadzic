// Load environment variable from .env file if not supplied
const path = require('path');
const jwt = require('jsonwebtoken');
require('dotenv').config({
  path:
    typeof process.env.DOTENV_PATH !== 'undefined'
      ? path.resolve(process.cwd(), process.env.DOTENV_PATH)
      : path.resolve(process.cwd(), '.env'),
});

// --- Start ---
const express = require('express');
const morgan = require('morgan');
const jwtExpress = require('express-jwt');

const loadPrivateKey = require('./utils/loadPrivateKey');

const { certPrivate, certPublic } = loadPrivateKey();

const app = express();

app.use(morgan('combined'));

app.get('/', (req, res) => {
  res.send('Home Page');
});

app.get('/users', (req, res) => {
  res.send('Users Page!');
});

app.get(
  '/my-profile',
  jwtExpress({
    secret: certPublic,
    algorithms: ['RS256'],
    requestProperty: 'auth', // Default is 'user'
  }),
  (req, res, next) => {
    if (!req.auth) {
      return res.sendStatus(403);
    }
    next();
  },
  (req, res) => {
    res.send(`Authorization successful! ${JSON.stringify(req.auth)}`);
  }
);

app.get('/public', (req, res) => {
  res.send(process.env.JWT_PUBLIC_KEY);
});

app.get('/sign/:id', (req, res) => {
  res.type('text/plain');
  const result = jwt.sign(
    {
      userId: req.params.id,
    },
    certPrivate,
    { algorithm: 'RS256' }
  );
  res.send(result);
});

app.get('*', function (req, res) {
  res.status(404).send('<h3>404 Page not Found</h3>');
});

const port = process.env.PORT;

app.listen(port, () => {
  console.log(`🚀 Server ready at http://localhost:${port}`);
});
